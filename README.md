# README #

Package contents:
Full android studio project. 

Compass Draw - Bitmap Draw utility with support for maintaining canvas direction regardless of device rotation using on board compass.

Features:

- Bitmap Paint like Draw utility using touch 
- Automatic Canvas rotation using on board compass
- Multiple colors and canvas clear via Fragment menu
- Persistent across application switch

Notes:
Ensure the on board compass has been calibrated prior to use.