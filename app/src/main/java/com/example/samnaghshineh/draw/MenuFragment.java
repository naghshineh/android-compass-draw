package com.example.samnaghshineh.draw;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/* MenuFragment handles linkage to color menu UI
 *   for the paint color as well as new canvas
 */
public class MenuFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        // Setup handles to view objects here
        // etFoo = (EditText) v.findViewById(R.id.etFoo);
        return view;
    }
}