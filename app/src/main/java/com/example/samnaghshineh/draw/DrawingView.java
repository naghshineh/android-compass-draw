package com.example.samnaghshineh.draw;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import java.lang.Math;
import android.graphics.PorterDuff;


public class DrawingView extends View {
    //drawing path
    private Path drawPath;
    //drawing and canvas paint
    private Paint drawPaint, canvasPaint;
    //initial color
    private int paintColor = 0xFF660000;
    //canvas
    private Canvas drawCanvas;
    //canvas bitmap
    private Bitmap canvasBitmap;
    private static float[] canvas_center = new float[2];


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        //view given size
        super.onSizeChanged(w, h, oldw, oldh);
        canvas_center[0] = w/2;
        canvas_center[1] = h/2;
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
    }

    // Used to clear canvas when the new canvas button is pressed
    public void startNew() {
        drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //detect user touch
        float currentAngle = MainActivity.currentDegree;

        /* Affix finger touch coordinate to the current orientation of canvas
         *
         * Note: Canvas is rotated in reference to its center while touch events
         *       are in relative to the top left corner of the screen.
         */

        // get touch coordinates relative to screen
        float touchX = event.getX();
        float touchY = event.getY();

        // convert to coordinates relative to center of screen
        float x_rel_center = touchX - canvas_center[0];
        float y_rel_center = touchY - canvas_center[1];

        // rotate relative to center of screen
        float touchX_rotated = x_rel_center*(float)Math.cos(Math.toRadians(-currentAngle)) - y_rel_center*(float)Math.sin(Math.toRadians(-currentAngle));
        float touchY_rotated = x_rel_center*(float)Math.sin(Math.toRadians(-currentAngle)) + y_rel_center*(float)Math.cos(Math.toRadians(-currentAngle));

        // convert back to coordinates relative to origin
        float touchX2 = touchX_rotated + canvas_center[0];
        float touchY2 = touchY_rotated + canvas_center[1];

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX2, touchY2);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX2, touchY2);
                break;
            case MotionEvent.ACTION_UP:
                drawCanvas.drawPath(drawPath, drawPaint);
                drawPath.reset();
                break;
            default:
                return false;
        }
        invalidate();
        return true;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        //draw view
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawPath(drawPath, drawPaint);
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupDrawing();
    }

    public void setColor(String newColor) {
        //set color
        invalidate();
        paintColor = Color.parseColor(newColor);
        drawPaint.setColor(paintColor);
    }

    private void setupDrawing() {
        //get drawing area setup for interaction
        drawPath = new Path();
        drawPaint = new Paint();
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(20);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        canvasPaint = new Paint(Paint.DITHER_FLAG);
        drawPaint.setStrokeWidth(20);

    }
}